# PandaBot 

import discord
from discord.ext import commands

import random

botprefix = "$" # change this to change the bot's prefix.

bot = commands.Bot(command_prefix = botprefix)
token = "YOUR_BOT_TOKEN_HERE"

@bot.event
async def on_message(message):
    if message.author == bot.user: return # bot messages are excluded
    
    await bot.process_commands(message) # if you decide to add on_message responses, this will allow commands to be used

@bot.command(pass_context=True)
async def hello(ctx):
    if ctx.message.author.id == 'YOUR_CLIENT_ID': person = ' my lord'
    else: person = ''

    return await bot.say(f'Hello there{person}, {ctx.message.author.mention}')

@bot.command()
async def prefix():
    return await bot.say(f"My prefix is `{botprefix}`.")

@bot.command()
async def cookie():
    return await bot.say("Here's a free cookie! :cookie:") # to return custom emojis you should use unicode emojis

@bot.command()
async def panda():
    return await bot.say("Here's a free portrait of myself! :panda_face:") # to return custom emojis you should use unicode emojis

@bot.command()
async def dynamic():
	string = random.choice(
            ["dynamic TOY system !",
             "backpack system DYNAMIC",
             "dynamic HALAL machine",
             "dynamic MINEcraft KEY ! Generator!"])

	await bot.say(string) # also a very basic example of random string generation.

@bot.command()
async def eightball(*, arg):
    string = random.choice(
        ["certain",
         "likely",
         "somewhat possible",
         "unlikely",
         "highly unlikely",
         "uncertain",
         "impossible"])

    return await bot.say(f"The 8ball says that your outcome of {arg} is '{string}'.") # very basic 8ball command. i would reccomend some modifications be done to it

@bot.command(pass_context=True)
async def guildname(ctx):
    return await bot.say(f"This is the '{ctx.message.server.name}' guild.")

@bot.event 
async def on_ready():
    print(f"> {bot.user.name} loading...")
    print("> Token loading...")
    print("> Script loaded in successfully.")
    print(f"> Bot name: {bot.user.name}")
    print(f"> Bot ID: {bot.user.id}")

    return await bot.change_presence(game=discord.Game(name="something...")) # rich presence for the bot. you can turn this on/off by commenting the line

bot.run(token) 
